import MapReduce
import sys

"""
Word Count Example in the Simple Python MapReduce Framework
"""

mr = MapReduce.MapReduce()

# =============================
# Do not modify above this line

def mapper(record):
    # key: document identifier
    # value: document contents
    #print record
    person = record[0]
    friend = record[1]
    mr.emit_intermediate(person,friend)
    mr.emit_intermediate(person, record)

def reducer(key, list_of_values):
    # key: word
    # value: list of occurrence counts
    print key, list_of_values
    #[mr.emit((key,p)) for p in list_of_values]
    #mr.emit((key, sum(list_of_values)))

# Do not modify below this line
# =============================
if __name__ == '__main__':
  inputdata = open(sys.argv[1])
  mr.execute(inputdata, mapper, reducer)
