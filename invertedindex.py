import MapReduce
import sys

"""
Word Count Example in the Simple Python MapReduce Framework
"""

mr = MapReduce.MapReduce()

# =============================
# Do not modify above this line

def mapper(record):
  # key: document identifier
  # value: document contents
  d_id = record[0]
  txt = record[1]
  words = txt.split()
  [mr.emit_intermediate(w,d_id) for w in words]

def reducer(key, list_of_values):
  # key: word
  # value: list of occurrence counts
  #print key, list_of_values
  #total = 0
  #for v in list_of_values:
  #  total += v
  noDupes = []
  [noDupes.append(i) for i in list_of_values if not noDupes.count(i)]
  mr.emit((key, noDupes))

# Do not modify below this line
# =============================
if __name__ == '__main__':
  inputdata = open(sys.argv[1])
  mr.execute(inputdata, mapper, reducer)
